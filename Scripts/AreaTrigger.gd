extends Area2D

export (String) var sceneName = "DangeonA"

func _on_AreaTrigger_body_entered(body):
	if body.get_name() == "Player":
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
