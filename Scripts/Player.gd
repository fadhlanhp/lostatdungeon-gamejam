extends KinematicBody2D

const SPEED = 200
const GRAVITY = 10
const JUMP_POWER = -500
const FLOOR = Vector2(0,-1)

var velocity = Vector2()

var on_ground = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _physics_process(delta):
	if on_ground == true:
	    velocity.x = SPEED
	    $AnimatedSprite.play("run")
	
	if Input.is_action_just_pressed("ui_right"):
		if on_ground == false:
			velocity.x += 100
	
	if Input.is_action_pressed("ui_up"):
	    if on_ground == true:
		    $AnimatedSprite.play("jump")
		    velocity.x = 250
		    velocity.y = JUMP_POWER
		    on_ground = false
	
		
	velocity.y += GRAVITY
	
	if is_on_floor():
		on_ground = true
	else:
		on_ground = false
		
	velocity = move_and_slide(velocity, FLOOR)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
